import React from "react";

class AppointmentForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      vin: "",
      name: "",
      date: "",
      time: "",
      reason: "",
      technician: "",
      technicians: [],
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  async componentDidMount() {
    const url = "http://localhost:8080/api/technician/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ technicians: data.technicians });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.technicians;

    const appUrl = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(appUrl, fetchConfig);
    if (response.ok) {
      window.location.href = "/service/appointments/list";
      // const newApp = await response.json();
      // console.log(newApp);

      // const cleared = {
      //   vin: "",
      //   name: "",
      //   date: "",
      //   time: "",
      //   reason: "",
      //   technician: "",
      // };
      // this.setState(cleared);
      // window.location.reload(false);
    }
  }

  handleInputChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({ ...this.state, [name]: value });
  };

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create an appointment</h1>
              <form onSubmit={this.handleSubmit} id="create-appointment-form">
                <div className="form-floating mb-3">
                  <input
                    value={this.state.vin}
                    onChange={this.handleInputChange}
                    placeholder="Color"
                    required
                    type="text"
                    name="vin"
                    id="vin"
                    className="form-control"
                  />
                  <label htmlFor="vin">VIN</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    value={this.state.name}
                    onChange={this.handleInputChange}
                    placeholder="Name"
                    required
                    type="text"
                    name="name"
                    id="name"
                    className="form-control"
                  />
                  <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    value={this.state.date}
                    onChange={this.handleInputChange}
                    placeholder="Date"
                    required
                    type="date"
                    name="date"
                    id="date"
                    className="form-control"
                  />
                  <label htmlFor="date">Date</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    value={this.state.time}
                    onChange={this.handleInputChange}
                    placeholder="Time"
                    required
                    type="time"
                    name="time"
                    id="time"
                    className="form-control"
                  />
                  <label htmlFor="time">Time</label>
                </div>

                <div className="form-floating mb-3">
                  <input
                    value={this.state.reason}
                    onChange={this.handleInputChange}
                    placeholder="Reason"
                    required
                    type="text"
                    name="reason"
                    id="reason"
                    className="form-control"
                  />
                  <label htmlFor="reason">Reason</label>
                </div>

                <div className="mb-3">
                  <select
                    value={this.state.technician}
                    onChange={this.handleInputChange}
                    required
                    name="technician"
                    id="technician"
                    className="form-select"
                  >
                    <option value="">Choose a Technician</option>
                    {this.state.technicians.map((technician) => {
                      return (
                        <option
                          key={technician.employee_number}
                          value={technician.employee_number}
                        >
                          {technician.name}
                        </option>
                      );
                    })}
                  </select>
                </div>
                <button className="btn btn-success">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AppointmentForm;
