import React from "react";

class SaleRecordsFilter extends React.Component {
  constructor() {
    super();
    this.state = {
      salespersons: [],
      sale_records: [],
      filtered_sale_records: [],
    };
    this.handleSalespersonChange = this.handleSalespersonChange.bind(this);
  }

  async componentDidMount() {
    const salespersonsResponse = await fetch(
      "http://localhost:8090/api/salespersons/"
    );
    const saleRecordsResponse = await fetch(
      "http://localhost:8090/api/salerecords/"
    );

    if (salespersonsResponse.ok && saleRecordsResponse.ok) {
      const salespersonsData = await salespersonsResponse.json();
      const saleRecordsData = await saleRecordsResponse.json();

      this.setState({ salespersons: salespersonsData.salespersons });
      this.setState({ sale_records: saleRecordsData.sale_records });
      this.setState({ filtered_sale_records: saleRecordsData.sale_records });
    }
  }

  handleSalespersonChange(event) {
    const value = event.target.value; // employee ID
    if (value !== "") {
      let employee_id = value;
      let sale_records = this.state.sale_records;
      let filtered_sale_records = [];
      for (let sale_record of sale_records) {
        if (sale_record.employee_id == employee_id) {
          filtered_sale_records.push(sale_record);
        }
      }
      this.setState({ filtered_sale_records: filtered_sale_records });
      this.setState({ salesperson: value });
    } else {
      let sale_records = this.state.sale_records;
      this.setState({ filtered_sale_records: sale_records });
      this.setState({ salesperson: value });
    }
  }

  render() {
    return (
        <div className="container">
          <h1>Sale history by salesperson</h1>
          <div className="mb-3">
            <select
              onChange={this.handleSalespersonChange}
              value={this.state.salesperson}
              required
              name="salesperson"
              id="salesperson"
              className="form-select"
            >
              <option value="">Select salesperson</option>
              {this.state.salespersons.map((salesperson) => {
                return (
                  <option
                    key={salesperson.employee_id}
                    value={salesperson.employee_id}
                  >
                    {salesperson.name +
                      " (ID: " +
                      salesperson.employee_id +
                      ")"}
                  </option>
                );
              })}
            </select>
          </div>
          <table className="table table-striped table-hover">
            <thead>
              <tr>
                <th>Salesperson</th>
                <th>Employee ID</th>
                <th>Customer</th>
                <th>VIN</th>
                <th>Sale price</th>
              </tr>
            </thead>
            <tbody>
              {this.state.filtered_sale_records.map((sale_record) => {
                return (
                  <tr key={sale_record.id}>
                    <td>{sale_record.employee}</td>
                    <td>{sale_record.employee_id}</td>
                    <td>{sale_record.customer}</td>
                    <td>{sale_record.vin}</td>
                    <td>{"$" + sale_record.price}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
    );
  }
}

export default SaleRecordsFilter;
