import React from "react";
import { Link } from "react-router-dom";

class PersonnelList extends React.Component {
  constructor() {
    super();
    this.state = {
      salespersons: [],
      technicians: [],
    };
  }

  async componentDidMount() {
    const salespersonsResponse = await fetch(
      "http://localhost:8090/api/salespersons/"
    );
    const techniciansResponse = await fetch(
      "http://localhost:8080/api/technician/"
    );

    if (salespersonsResponse.ok && techniciansResponse.ok) {
      const salespersonsData = await salespersonsResponse.json();
      const techniciansData = await techniciansResponse.json();

      this.setState({ salespersons: salespersonsData.salespersons });
      this.setState({ technicians: techniciansData.technicians });
    }
  }

  render() {
    return (
        <div className="container">
          <h1>Personnel</h1>
          <div className="container">
            <div className="row">
              <div className="col">
                <h3>Salespeople</h3>
                <Link
                  to="/personnel/salespersons/new"
                  className="btn btn-primary btn-md px-4 gap-3"
                >
                  Add a salesperson
                </Link>
                <table className="table table-striped table-hover">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Employee ID</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.salespersons.map((salesperson) => {
                      return (
                        <tr key={salesperson.id}>
                          <td>{salesperson.name}</td>
                          <td>{salesperson.employee_id}</td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
              <div className="col">
                <h3>Technicians</h3>
                <Link
                  to="/personnel/technicians/new"
                  className="btn btn-primary btn-md px-4 gap-3"
                >
                  Add a technician
                </Link>
                <table className="table table-striped table-hover">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Employee ID</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.technicians.map((technician) => {
                      return (
                        <tr key={technician.id}>
                          <td>{technician.name}</td>
                          <td>{technician.employee_number}</td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
    );
  }
}

export default PersonnelList;
