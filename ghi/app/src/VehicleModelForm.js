import React from "react";

class VehicleModelForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      manufacturers: [],
      picture_url: "",
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() {
    const url = "http://localhost:8100/api/manufacturers/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ manufacturers: data.manufacturers });
    }
  }

  handleInputChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({ ...this.state, [name]: value });
  };

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.manufacturers;

    const vehicleModelUrl = "http://localhost:8100/api/models/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(vehicleModelUrl, fetchConfig);
    if (response.ok) {
      window.location.href = "/inventory/models/list";
    }
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a new model</h1>
              <form onSubmit={this.handleSubmit} id="create-model-form">
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleInputChange}
                    value={this.state.name}
                    placeholder="Model name"
                    required
                    type="text"
                    name="name"
                    id="name"
                    className="form-control"
                  />
                  <label htmlFor="name">Model name</label>
                </div>
                <div className="form-floating mb-3">
                  <select
                    onChange={this.handleInputChange}
                    value={this.state.manufacturer_id}
                    placeholder="Model name"
                    required
                    name="manufacturer_id"
                    id="manufacturer_id"
                    className="form-select"
                  >
                    <option value="">Select manufacturer</option>
                    {this.state.manufacturers.map((manufacturer) => {
                      return (
                        <option key={manufacturer.id} value={manufacturer.id}>
                          {manufacturer.name}
                        </option>
                      );
                    })}
                  </select>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleInputChange}
                    value={this.state.picture_url}
                    placeholder="Picture URL"
                    required
                    type="url"
                    name="picture_url"
                    id="picture_url"
                    className="form-control"
                  />
                  <label htmlFor="picture_url">Picture URL</label>
                </div>
                <button className="btn btn-success">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default VehicleModelForm;
