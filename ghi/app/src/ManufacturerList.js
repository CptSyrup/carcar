import React from "react";
import { Link } from "react-router-dom";

class ManufacturersList extends React.Component {
  constructor() {
    super();
    this.state = { manufacturers: [] };
  }

  async componentDidMount() {
    const response = await fetch("http://localhost:8100/api/manufacturers/");
    if (response.ok) {
      const data = await response.json();
      this.setState({ manufacturers: data.manufacturers });
    } else {
      console.error(response);
    }
  }

  render() {
    return (
        <div className="container">
          <h1>Manufacturers</h1>
          <div>
            <Link
              to="/inventory/manufacturers/new"
              className="btn btn-primary btn-md px-4 gap-3"
            >
              Add a manufacturer
            </Link>
          </div>
          <table className="table table-striped table-hover">
            <thead>
              <tr>
                <th>Name</th>
              </tr>
            </thead>
            <tbody>
              {this.state.manufacturers.map((manufacturer) => {
                return (
                  <tr key={manufacturer.id}>
                    <td>{manufacturer.name}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
    );
  }
}

export default ManufacturersList;
