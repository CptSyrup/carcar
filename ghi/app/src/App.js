import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ManufacturersList from "./ManufacturerList";
import ManufacturerForm from "./ManufacturerForm";
import AutomobileForm from "./AutomobileForm";
import AutomobilesList from "./AutomobileList";
import VehicleModelsList from "./VehicleModelList";
import VehicleModelForm from "./VehicleModelForm";
import AppointmentForm from "./AppointmentForm";
import TechnicianForm from "./TechnicianForm";
import AppointmentList from "./AppointmentList";
import AppointmentHistory from "./AppointmentHistory";
import SalespersonForm from "./SalespersonForm";
import CustomerForm from "./CustomerForm";
import SaleRecordForm from "./SaleRecordForm";
import SaleRecordsList from "./SaleRecordList";
import SaleRecordsFilter from "./SaleRecordFilter";
import UnsoldAutomobilesList from "./UnsoldAutomobileList";
import PersonnelList from "./PersonnelList";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div>
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="inventory">
            <Route path="manufacturers">
              <Route path="list" element={<ManufacturersList />} />
              <Route path="new" element={<ManufacturerForm />} />
            </Route>

            <Route path="automobiles">
              <Route path="all" element={<AutomobilesList />} />
              <Route path="new" element={<AutomobileForm />} />
              <Route path="unsold" element={<UnsoldAutomobilesList />} />
            </Route>

            <Route path="models">
              <Route path="list" element={<VehicleModelsList />} />
              <Route path="new" element={<VehicleModelForm />} />
            </Route>
          </Route>

          <Route path="sales">
            <Route path="customers/new" element={<CustomerForm />} />
            <Route path="salerecords">
              <Route path="new" element={<SaleRecordForm />} />
              <Route path="list" element={<SaleRecordsList />} />
              <Route path="filter" element={<SaleRecordsFilter />} />
            </Route>
          </Route>

          <Route path="service">
            <Route path="appointments">
              <Route path="new" element={<AppointmentForm />} />
              <Route path="list" element={<AppointmentList />} />
              <Route path="history" element={<AppointmentHistory />} />
            </Route>
          </Route>

          <Route path="personnel">
            <Route path="list" element={<PersonnelList />} />
            <Route path="salespersons/new" element={<SalespersonForm />} />
            <Route path="technicians/new" element={<TechnicianForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
