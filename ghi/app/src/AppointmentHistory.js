import React from "react";
import { Link } from "react-router-dom";

class ServiceHistoryList extends React.Component {
  constructor() {
    super();
    this.state = { searchVin: "", appointments: [], extraList: [] };
  }

  async componentDidMount() {
    const response = await fetch("http://localhost:8080/api/appointments/");
    if (response.ok) {
      const data = await response.json();
      const filterData = data.appointments.filter(
        (appointment) => appointment.completed === true
      );
      this.setState({ appointments: filterData, extraList: filterData });
    } else {
      console.error(response);
    }
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    const data = { ...this.state };
    const vinData = data.appointments.filter(
      (appointment) => appointment.vin === data.searchVin
    );
    if (data.searchVin) {
      this.setState({ appointments: vinData });
    } else {
      this.setState({ appointments: data.extraList });
    }
  };

  handleInputChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({ ...this.state, [name]: value });
  };

  render() {
    return (
      <React.Fragment>
        <div className="container">
          <h1>Appointment history</h1>
          <form onSubmit={this.handleSubmit}>
            <div className="input-group mb-3 p-4">
              <input
                onChange={this.handleInputChange}
                value={this.state.searchVin}
                id="searchVin"
                name="searchVin"
                type="text"
                className="form-control"
                placeholder="VIN"
              />
              <div className="px-4">
                <button className="btn btn-primary" type="submit">
                  Search
                </button>
              </div>
            </div>
          </form>

          <table className="table table-striped table-hover">
            <thead>
              <tr>
                <th>VIN</th>
                <th>Name</th>
                <th>Date</th>
                <th>Time</th>
                <th>Reason</th>
                <th>Technician</th>
              </tr>
            </thead>
            <tbody>
              {this.state.appointments.map((appointment) => {
                return (
                  <tr key={appointment.id}>
                    <td>{appointment.vin}</td>
                    <td>{appointment.name}</td>
                    <td>{appointment.date}</td>
                    <td>{appointment.time}</td>
                    <td>{appointment.reason}</td>
                    <td>{appointment.tech_name}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </React.Fragment>
    );
  }
}

export default ServiceHistoryList;
