import React from "react";

class AutomobileForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      color: "",
      year: "",
      vin: "",
      models: [],
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  async componentDidMount() {
    const url = "http://localhost:8100/api/models/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ models: data.models });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.models;

    const autoUrl = "http://localhost:8100/api/automobiles/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(autoUrl, fetchConfig);
    if (response.ok) {
      window.location.href = "/inventory/automobiles/all";
    }
  }

  handleInputChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({ ...this.state, [name]: value });
  };

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a new automobile</h1>
              <form onSubmit={this.handleSubmit} id="create-shoe-form">
                <div className="form-floating mb-3">
                  <input
                    value={this.state.color}
                    onChange={this.handleInputChange}
                    placeholder="Color"
                    required
                    type="text"
                    name="color"
                    id="color"
                    className="form-control"
                  />
                  <label htmlFor="Color">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    value={this.state.year}
                    onChange={this.handleInputChange}
                    placeholder="Year"
                    required
                    type="number"
                    name="year"
                    id="year"
                    className="form-control"
                  />
                  <label htmlFor="year">Year</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    value={this.state.vin}
                    onChange={this.handleInputChange}
                    placeholder="Vin"
                    required
                    type="text"
                    name="vin"
                    id="vin"
                    className="form-control"
                  />
                  <label htmlFor="vin">VIN</label>
                </div>

                <div className="mb-3">
                  <select
                    value={this.state.model_id}
                    onChange={this.handleInputChange}
                    required
                    name="model_id"
                    id="model_id"
                    className="form-select"
                  >
                    <option value="">Choose a Model</option>
                    {this.state.models.map((model) => {
                      return (
                        <option key={model.id} value={model.id}>
                          {model.manufacturer.name + " " + model.name}
                        </option>
                      );
                    })}
                  </select>
                </div>
                <button className="btn btn-success">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AutomobileForm;
