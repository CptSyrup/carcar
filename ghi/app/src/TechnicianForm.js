import React from "react";

class TechnicianForm extends React.Component {
  constructor() {
    super();
    this.state = {
      name: "",
      employee_number: "",
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({ ...this.state, [name]: value });
  };

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };

    const techUrl = "http://localhost:8080/api/technician/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(techUrl, fetchConfig);
    if (response.ok) {
      window.location.href = "/personnel/list";
      // const newTech = await response.json();
      // console.log(newTech);

      // const cleared = {
      //   name: "",
      //   employee_number: "",
      // };
      // this.setState(cleared);
    }
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a new technician</h1>
              <form onSubmit={this.handleSubmit} id="create-manufacturer-form">
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleInputChange}
                    value={this.state.name}
                    placeholder="Technician name"
                    required
                    type="text"
                    name="name"
                    id="name"
                    className="form-control"
                  />
                  <label htmlFor="name">Technician name</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleInputChange}
                    value={this.state.employee_number}
                    placeholder="Manufacturer name"
                    required
                    type="text"
                    name="employee_number"
                    id="employee_number"
                    className="form-control"
                  />
                  <label htmlFor="employee_number">Employee number</label>
                </div>
                <button className="btn btn-success">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default TechnicianForm;
