import React from "react";

class SaleRecordForm extends React.Component {
  constructor() {
    super();
    this.state = {
      automobiles: [],
      salespersons: [],
      customers: [],
      price: "",
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() {
    const automobilesResponse = await fetch(
      "http://localhost:8090/api/automobiles/unsold/"
    );
    const salespersonsResponse = await fetch(
      "http://localhost:8090/api/salespersons/"
    );
    const customersResponse = await fetch(
      "http://localhost:8090/api/customers/"
    );

    if (
      automobilesResponse.ok &&
      salespersonsResponse.ok &&
      customersResponse.ok
    ) {
      const automobilesData = await automobilesResponse.json();
      const salespersonsData = await salespersonsResponse.json();
      const customersData = await customersResponse.json();

      this.setState({ automobiles: automobilesData.automobiles });
      this.setState({ salespersons: salespersonsData.salespersons });
      this.setState({ customers: customersData.customers });
    }
  }

  handleInputChange(event) {
    const { value, name } = event.target;
    this.setState({ [name]: value });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    delete data.automobiles;
    delete data.salespersons;
    delete data.customers;

    const saleRecordUrl = "http://localhost:8090/api/salerecords/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(saleRecordUrl, fetchConfig);
    if (response.ok) {
      const newSaleRecord = await response.json();
      window.location.href = "/sales/salerecords/list";
    }
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Record a new sale</h1>
              <form onSubmit={this.handleSubmit} id="create-sales-record-form">
                <div className="mb-3">
                  <select
                    onChange={this.handleInputChange}
                    value={this.state.automobile}
                    required
                    name="automobile"
                    id="automobile"
                    className="form-select"
                  >
                    <option value="">Select automobile</option>
                    {this.state.automobiles.map((automobile) => {
                      return (
                        <option key={automobile.vin} value={automobile.vin}>
                          {automobile.vin}
                        </option>
                      );
                    })}
                  </select>
                </div>
                <div className="mb-3">
                  <select
                    onChange={this.handleInputChange}
                    value={this.state.salesperson}
                    required
                    name="salesperson"
                    id="salesperson"
                    className="form-select"
                  >
                    <option value="">Select salesperson</option>
                    {this.state.salespersons.map((salesperson) => {
                      return (
                        <option
                          key={salesperson.employee_id}
                          value={salesperson.employee_id}
                        >
                          {salesperson.name +
                            " (ID: " +
                            salesperson.employee_id +
                            ")"}
                        </option>
                      );
                    })}
                  </select>
                </div>
                <div className="mb-3">
                  <select
                    onChange={this.handleInputChange}
                    value={this.state.customer}
                    required
                    name="customer_id"
                    id="customer_id"
                    className="form-select"
                  >
                    <option value="">Select customer</option>
                    {this.state.customers.map((customer) => {
                      return (
                        <option key={customer.id} value={customer.id}>
                          {customer.name}
                        </option>
                      );
                    })}
                  </select>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={this.handleInputChange}
                    value={this.state.price}
                    placeholder="Price"
                    required
                    type="currency"
                    name="price"
                    id="price"
                    className="form-control"
                  />
                  <label htmlFor="price">Price</label>
                </div>
                <button className="btn btn-success">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SaleRecordForm;
