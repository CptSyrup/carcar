from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import (
    AutomobileVOEncoder,
    SalespersonEncoder,
    CustomerEncoder,
    SaleRecordEncoder,
)

from .models import AutomobileVO, Salesperson, Customer, SaleRecord

@require_http_methods(["GET", "POST"])
def api_salespersons(request):
    if request.method == "GET":
        salespersons = Salesperson.objects.all()
        return JsonResponse(
            {"salespersons": salespersons},
            encoder=SalespersonEncoder
        )
    else:  # POST
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create salesperson"}
            )
            response.status_code = 400
            return response 

@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder
        )
    else:  # POST
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create customer"}
            )
            response.status_code = 400
            return response 

@require_http_methods(["GET", "POST"])
def api_sale_records(request):
    if request.method == "GET":
        sale_records = SaleRecord.objects.all()
        return JsonResponse(
            {"sale_records": sale_records},
            encoder=SaleRecordEncoder,
        )
    else:  # POST
        content = json.loads(request.body)
        try:
            vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=vin)
            content["automobile"] = automobile
        except:
            response = JsonResponse(
                {"message": "issue with automobile"}
            )
            response.status_code = 400
            return response
            
        try:
            employee_id = content["salesperson"]
            employee = Salesperson.objects.get(employee_id=employee_id)
            content["salesperson"] = employee
        except:
            response = JsonResponse(
                {"message": "issue with employee"}
            )
            response.status_code = 400
            return response
        try:
            customer_id = content["customer_id"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
        except:
            response = JsonResponse(
                {"message": "issue with customer"}
            )
            response.status_code = 400
            return response

        sale_record = SaleRecord.objects.create(**content)
        return JsonResponse(
            sale_record,
            encoder=SaleRecordEncoder,
            safe=False,
        )

@require_http_methods("GET")
def api_unsold_automobiles(request):
    sold_vins = [sale_record.automobile.vin for sale_record in SaleRecord.objects.all()]
    unsold = AutomobileVO.objects.exclude(vin__in=sold_vins)
    return JsonResponse(
        {"automobiles": unsold},
        encoder=AutomobileVOEncoder,
        safe=False,
    )

