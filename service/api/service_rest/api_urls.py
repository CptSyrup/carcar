from django.urls import path
from .views import api_list_appointments, api_technician, api_show_appointments

urlpatterns = [
     path("appointments/", api_list_appointments, name="api_list_appointments"),
     path("technician/", api_technician, name="api_technician"),
     path("appointments/<int:pk>/", api_show_appointments, name="api_show_appointments")

    ]
