from django.db import models
from django.urls import reverse

# Create your models here.

#Automobile value object model:

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, default=None)
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return self.vin
    
#Technician Model:

class Technician(models.Model):
    name = models.CharField(max_length=100, null=True)
    employee_number = models.PositiveIntegerField(null=True, unique=True)

    def __str__(self):
        return self.name
    
    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.id})

#Service Appointment Model:

class Appointment(models.Model):
    vin = models.CharField(max_length=17)
    name = models.CharField(max_length=100)
    date = models.DateField(null=True)
    time = models.TimeField(null=True)
    reason = models.CharField(max_length=250)
    vip = models.BooleanField(default=False)
    completed = models.BooleanField(default=False)

    technician = models.ForeignKey(
        Technician, 
        related_name="appointment",
        on_delete=models.PROTECT,
        null=True,
    )

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_list_appointments", kwargs={"pk": self.id})




