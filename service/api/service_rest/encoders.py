from common.json import ModelEncoder
from .models import Appointment, AutomobileVO, Technician

#Ecoders:

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
        "id"
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "name",
        "date",
        "time",
        "reason",
        "vip",
        "completed",
        "id",
    ]

    def get_extra_data(self, o):
        return {
            "tech_name": o.technician.name
        }
        
    encoders = {
        "technician": TechnicianEncoder,
    }
   
    
class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "name",
        "date",
        "time",
        "reason",
        "technician",
        "vip",
        "completed",
    ]
    encoders = {
        "technician": TechnicianEncoder,
    }
    

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "color",
        "year",
        "vin",
    ]
    